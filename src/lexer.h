#include <string.h>
#include <stdio.h>
#include "readFile.h"

typedef struct TOKEN TOKEN;
struct TOKEN {
  int token;
  // TODO: make non limited size of value
  char value[255];
  int start_position;
  int end_position;
};

enum tokens {
  UNKNOWN,
  IDENTIFIER,
  DIGIT,
  STRING,
  KEYWORD,
  COMMENT,
  OPERANBR, // (
  CPERANBR, // )
  OSQRBR, // [
  CSQRBR, // ]
  OCURLBR, // {
  CCURLBR // }
};

TOKEN *lexer();
void lexer_debug();
char *readFile ();

int main (int argc, char *argv[]) {
  if (argc == 2)
    lexer(readFile(argv[1]));
  return 0;
}

int isDigit (char target) {
  return target >= '0' && target <= '9'
    ? 1
    : 0;
}

int isLeter (char target) {
  return target >= 'a' && target <= 'z'
    || target >= 'A' && target <= 'Z'
    ? 1
    : 0;
}

int ignoreCharSet (char target) {
  return target == ' '
    || target == '\n'
    ? 1
    : 0;
}

int ignoreCharSet (char target) {
  return target == ' '
    || target == '\n'
    ? 1
    : 0;
}

int identifierCharSet (char target) {
  return target == '+'
    || target == '-'
    || target == '/'
    || target == '^'
    || target == '$'
    || target == '#'
    || target == '@'
    || isDigit(target)
    || isLeter(target)
    ? 1
    : 0;
}

void lexer_debug (TOKEN *lexer, int size) {
  for (int i = 0; i <= size; i++){
    printf("lexeme %d value %s token %d stp %d edp %d\n",
      i,
      lexer[i].value,
      lexer[i].token,
      lexer[i].start_position,
      lexer[i].end_position);
  }
}

// TODO: make retuen data; now void (debug only)
TOKEN *lexer (char *source) {
  // TODO: make not limited token list
  TOKEN lexemes[255];
  int lexemes_index;
  // TODO: optimize buffer
  char buffer[255];
  char cursor;
  int index = 0;
  int is_end = 1;
  int start_position, end_position;

  while (is_end) {
    cursor = (char) source[index++];
    if (ignoreCharSet(cursor)) {
      // DO NOTHIN
    } else if (isDigit(cursor)) {
      /* DIGITS */
      start_position = index;
      while (isDigit(cursor)) {
        buffer[strlen(buffer)] = cursor;
        cursor = (char) source[index++];
      }
      end_position = index;
      lexemes[lexemes_index] = (TOKEN) {
        .token = DIGIT,
        .start_position = start_position,
        .end_position = end_position
      };
      strcpy(lexemes[lexemes_index].value, buffer);
      memset(buffer, 0, sizeof buffer);
      lexemes_index++;
    } else if (identifierCharSet(cursor)) {
      /* IDENTIFIER */
      start_position = index;
      while (identifierCharSet(cursor)) {
        buffer[strlen(buffer)] = cursor;
        cursor = (char) source[index++];
      }
      end_position = index;
      lexemes[lexemes_index] = (TOKEN) {
        .token = IDENTIFIER,
        .start_position = start_position,
        .end_position = end_position
      };
      strcpy(lexemes[lexemes_index].value, buffer);
      memset(buffer, 0, sizeof buffer);
      lexemes_index++;
    } else if (cursor == '"') {
      /* STRING */
      cursor = (char) source[index++];
      start_position = index;
      while (cursor != '"') {
        buffer[strlen(buffer)] = cursor;
        cursor = (char) source[index++];
      }
      end_position = index;
      lexemes[lexemes_index] = (TOKEN) {
        .token = STRING,
        .start_position = start_position,
        .end_position = end_position
      };
      strcpy(lexemes[lexemes_index].value, buffer);
      memset(buffer, 0, sizeof buffer);
      lexemes_index++;
    } else if (cursor == ':') {
      /* KEYWORD */
      start_position = index;
      while (identifierCharSet(cursor) || cursor == ':') {
        buffer[strlen(buffer)] = cursor;
        cursor = (char) source[index++];
      }
      end_position = index;
      lexemes[lexemes_index] = (TOKEN) {
        .token = KEYWORD,
        .start_position = start_position,
        .end_position = end_position
      };
      strcpy(lexemes[lexemes_index].value, buffer);
      memset(buffer, 0, sizeof buffer);
      lexemes_index++;
    } else if (cursor == ';') {
      /* COMMENT */
      cursor = (char) source[index++];
      start_position = index;
      while (cursor != '\n') {
        buffer[strlen(buffer)] = cursor;
        cursor = (char) source[index++];
      }
      end_position = index;
      lexemes[lexemes_index] = (TOKEN) {
        .token = COMMENT,
        .start_position = start_position,
        .end_position = end_position
      };
      strcpy(lexemes[lexemes_index].value, buffer);
      memset(buffer, 0, sizeof buffer);
      lexemes_index++;
    } else if (cursor == '(' ) {
      lexemes[lexemes_index] = (TOKEN) {
        .token = OPERANBR,
        .value = '(',
        .start_position = index,
        .end_position = index
      };
      lexemes_index++;
    } else if (cursor == ')') {
      lexemes[lexemes_index] = (TOKEN) {
        .token = CPERANBR,
        .value = ')',
        .start_position = index,
        .end_position = index
      };
      lexemes_index++;
    } else if (cursor == '[') {
      lexemes[lexemes_index] = (TOKEN) {
        .token = OSQRBR,
        .value = '[',
        .start_position = index,
        .end_position = index
      };
      lexemes_index++;
    } else if (cursor == ']') {
      lexemes[lexemes_index] = (TOKEN) {
        .token = CSQRBR,
        .value = ']',
        .start_position = index,
        .end_position = index
      };
      lexemes_index++;
    } else if (cursor == '{') {
      lexemes[lexemes_index] = (TOKEN) {
        .token = OCURLBR,
        .value = '{',
        .start_position = index,
        .end_position = index
      };
      lexemes_index++;
    } else if (cursor == '}') {
      lexemes[lexemes_index] = (TOKEN) {
        .token = CCURLBR,
        .value = '}',
        .start_position = index,
        .end_position = index
      };
      lexemes_index++;
    } else if (cursor == '\0') {
      is_end = 0;
    } else {
      lexemes[lexemes_index] = (TOKEN) {
        .token = UNKNOWN,
        .value = cursor,
        .start_position = index,
        .end_position = index
      };
      lexemes_index++;
    }
  }
  lexer_debug(lexemes, 24);
}
