# TODO
* Make ast parser
* Сhoose the language of compilation
* Make Repl
    * Handle last out put into memory
    * Repl work in context of project (in folder in which you are running)
* Make compiler
* Jit compiling
* Implemented self docing of function
    * Implemented docmentation comment
    * Added function test case
* Realize control ctructure
    * Implemented modules of code
    * Implemented namespace
    * Implemented lambda function
    * Implemented varible definition
    * Implemented simple comment
    * Implemented macro system
* Make cli for start project deployment  
* Complete lexer (added all necessary data type)
    * Arrays
    * Float
    * Integer
    * Binary
    * Char
* Complete README

# TODO: to amend
* Make retuen data; now void (debug only) *** lexer.c : 63 ***
* Make not limited token list *** lexer.c : 65 ***
* Optimize buffer *** lexer.c : 68 ***
* Added any character support for identifier *** lexer.c : 94 ***
