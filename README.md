# E - programming language
> current version: v 0.0.170

## *DEPRECATED*

Now idea of this language is useles.
For more information see [GraalVM](http://www.graalvm.org)

# Concept
E - low lever scripting language that have base option:
* low level memory access
* function definition
* variable definition
* macros definition

# In future
* Added self docing of language

> Example
>```clojure
(defn FUNC_NAME [ARG1 ARG2 ARG3]
  "Base description for function
   that will be added as Description
   in auto generated documentation"
  (:test-case
   [EXPECTED_VALUE1]
   [RES_ARG_1 RES_ARG_1 RES_ARG_1]
   [EXPECTED_VALUE2]
   [RES_ARG_2 RES_ARG_2 RES_ARG_2])
  (print "hellow world")
  (+ 2 2))
```
