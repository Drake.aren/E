# v 0.0.170
* lexer identifier STRING
* lexer identifier COMMENT
* lexer identifier KEYWORD

# v 0.0.140
* lexer identifier IDENTIFIER's
* lexer identifier DIGIT's
* lexer identifier brakets

# v 0.0.100
* added base `e-lexer`
